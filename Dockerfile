FROM python:3.7

RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
EXPOSE 5000
ENV FLASK_APP=main.py

ENV DB_HOST 36.255.68.119
ENV DB_USER dudhwala

COPY . /app

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "main:app", "--log-level=debug"]
