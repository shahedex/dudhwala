from typing import List, Dict, Union, Optional

from src.models.user import User


class UserRepository(object):

    @staticmethod
    def create_user(name: str, phone: str, password: str, regular: bool, **kwargs) -> Optional[User]:
        existing_user = UserRepository.get_by_phone(phone)
        if existing_user:
            return None

        new_user = User(name, phone, password, regular, **kwargs)
        return new_user.save()

    @staticmethod
    def update_user(user: User, **kwargs) -> User:
        updatable_columns = user.updatable_columns()
        for key in kwargs.keys():
            if key not in updatable_columns:
                kwargs.pop(key)

        return user.update(**kwargs)

    @staticmethod
    def get_all() -> List[User]:
        return User.query.all()

    @staticmethod
    def get_by_id(id: int) -> User:
        # TODO: implement mechanism to get user form cache
        return User.query.get(id)

    @staticmethod
    def get_by_email(email: str) -> User:
        return User.query.filter(
            User.email == email
        ).first()
    
    @staticmethod
    def get_by_phone(phone: str) -> User:
        return User.query.filter(
            User.phone == phone
        ).first()
