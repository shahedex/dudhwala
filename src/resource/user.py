from flask import Blueprint, request
from flask_jwt_extended import jwt_required, current_user
from flask_restful import Api, Resource

from src.decorators.requests import json_data_required
from src.repository.user_token import UserToken
from src.repository.user import UserRepository
from src.utils import ResponseGenerator

user_blueprint = Blueprint(name="userbp", import_name=__name__, url_prefix="/api/v1/user")
api = Api(app=user_blueprint)


class UserRegister(Resource):
    method_decorators = [json_data_required]

    def post(self):
        data = request.get_json()

        mandatory_fields = ["name", "phone", "password", "regular"]
        if any(data.get(item) is None for item in mandatory_fields):
            return ResponseGenerator.mandatory_field(fields=mandatory_fields)

        name, phone, password, regular = data.pop("name"), data.pop("phone"), data.pop("password"), data.pop("regular")
        user = UserRepository.create_user(name=name, phone=phone, password=password, regular=regular, **data)
        if not user:
            return ResponseGenerator.error_response(msg="Phone Number is already registered, try to login", code=400)

        access_token = UserToken.create_user_access_token(user=user)
        return ResponseGenerator.generate_response({
            "access_token": access_token
        }, code=201)


class UserLogin(Resource):
    method_decorators = [json_data_required]

    def post(self):
        data = request.get_json()

        mandatory_fields = ["phone", "password"]
        if any(data.get(item) is None for item in mandatory_fields):
            return ResponseGenerator.mandatory_field(fields=mandatory_fields)

        phone = data["phone"]
        password = data["password"]

        user = UserRepository.get_by_phone(phone=phone)
        if not user:
            return ResponseGenerator.not_found(msg="user not found")

        if not user.check_password(password=password):
            return ResponseGenerator.forbidden(msg="phone/password combination is invalid")

        access_token = UserToken.create_user_access_token(user=user)
        return ResponseGenerator.generate_response({
            "access_token": access_token
        }, code=200)


class UserProfile(Resource):

    @jwt_required
    def get(self):
        return ResponseGenerator.generate_response(current_user.json, code=200)


api.add_resource(UserRegister, "/register")
api.add_resource(UserLogin, "/login")
api.add_resource(UserProfile, "/authenticate")
