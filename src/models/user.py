import bcrypt

from src.models.base import BaseModel
from src.core.database import db
from src.utils import Generator


class User(db.Model, BaseModel):
    __tablename__ = "user"

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(100), unique=True, index=True, nullable=False)
    email = db.Column(db.String(100), unique=False, nullable=True)
    regular = db.Column(db.Boolean, nullable=False, default=False)
    _password = db.Column(db.String(128), nullable=False)
    avatar = db.Column(db.Text, nullable=True)


    def __init__(self, name, phone, password, regular, **kwargs):
        super(User, self).__init__(**kwargs)
        """ Create a new User """
        self.name = name
        self.phone = phone
        self.regular = regular
        self._password = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

    def __repr__(self):
        return "User::{}".format(self.id)

    def check_password(self, password: str):
        return bcrypt.checkpw(password=password.encode(), hashed_password=self._password.encode())
